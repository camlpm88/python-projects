import json
from difflib import get_close_matches

data = json.load(open("data.json"))

def translate(word):
  word = word.lower()
  if word in data:
    return data[word]
  elif len(get_close_matches(word, data.keys())):
    yn = input("Did you mean to search for '%s' instead? Enter 'Y' or 'N': " % get_close_matches(word, data.keys())[0])
    if yn == "Y":
      return data[get_close_matches(word, data.keys())[0]]
    elif yn == "N":
      return "Sorry, your word doesn't exist in this dictionary."
    else:
      return "Sorry, your entry isn't recognized."
  else:
    return "The word is undefined. Please double-check."

word = input("Enter word: ")

output = translate(word)

if type(output) == list:
  for item in output:
    print(item)
else:
  print(output)